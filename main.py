import argparse
from simu import Simu
from dash_frontend import DashFrontend


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="Modify StarPU scheduling visually"
    )
    parser.add_argument("tasks_file", metavar="tasks.rec",
                        help="Task graph provided by StarPU")
    parser.add_argument("-s", "--scheduler", default="dmdas",
                        help="Name of the scheduler (default: dmdas)")
    parser.add_argument("-H", "--host", "--hostname", default="mirage",
                        help="Hostname of the simulated machine (default: mirage)")
    parser.add_argument("-m", "--perf-model-dir",
                        default="/usr/local/share/starpu/perfmodels/sampling",
                        help="StarPU perf models directory (default: /usr/local/share/starpu/perfmodels/sampling)")
    args = parser.parse_args()

    simu = Simu(task_graph=args.tasks_file,
                scheduler=args.scheduler,
                hostname=args.host,
                perf_model_dir=args.perf_model_dir)
    frontend = DashFrontend("Scheduling Taskinator", simu)
    frontend.run()
