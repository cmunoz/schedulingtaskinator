#!/bin/bash

if [ ! -d "venv" ]; then
  echo "New installation detected"
  ./setup.sh
fi

venv/bin/python3 main.py "$@"
