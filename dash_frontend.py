from dash import Dash, dcc, html, Input, Output, State, ctx, MATCH, ALL, Patch, no_update
from dash.exceptions import PreventUpdate
import plotly.graph_objects as go
from static_sched import Constraint, WorkerConstraint, DependencyConstraint, PriorityConstraint
from simu import Simu
import numpy as np


class DashFrontend:
    def __init__(self, name: str, simu: Simu):
        self.simu = simu
        self.app = Dash(name, title=name, prevent_initial_callbacks="initial_duplicate")
        self.app.config.suppress_callback_exceptions = True
        self.app.layout = html.Div([
            dcc.Store(id="notification"),
            dcc.Input(id="static-sched-submitorder", type="hidden"),
            dcc.Input(id="static-sched-dependency-click", type="hidden", value="false"),
            html.Nav(className="navbar", role="navigation", children=[
                html.Div(className="navbar-menu", children=[
                    html.Div(className="navbar-start", children=[
                        html.Div(className="navbar-item has-dropdown is-hoverable", children=[
                            html.A("Set Worker", className="navbar-link"),
                            html.Div(className="navbar-dropdown", children=[
                                html.Div(className="navbar-item", children=[
                                    html.Label("Worker:", htmlFor="static-sched-workerid"),
                                ]),
                                html.Div(className="navbar-item", children=[
                                    dcc.Dropdown(self._get_workers_dropdown_list(), id="static-sched-workerid",
                                                 style={"minWidth": "110px"})
                                ]),
                                html.Div(className="navbar-item", children=[
                                    html.Button("Set worker", type="button", id="static-sched-assign",
                                                className="button")
                                ])
                            ])
                        ]),
                        html.Div(className="navbar-item has-dropdown is-hoverable", children=[
                            html.A("Add dependency", className="navbar-link"),
                            html.Div(className="navbar-dropdown", children=[
                                html.Div(className="navbar-item", children=[
                                    html.Label("Parent SubmitOrder:", htmlFor="static-sched-parent"),
                                ]),
                                html.Div(className="navbar-item", children=[
                                    dcc.Input(id="static-sched-parent", type="number", min=0, className="input"),
                                ]),
                                html.Div(className="navbar-item", children=[
                                    html.Button("Add dependency", type="button", id="static-sched-add-dependency",
                                                className="button")
                                ]),
                                html.Hr(className="navbar-divider"),
                                html.Div(className="navbar-item", children=[
                                    html.Button("Click on a task", type="button",
                                                id="static-sched-add-dependency-click", className="button")
                                ])
                            ])
                        ]),
                        html.Div(className="navbar-item has-dropdown is-hoverable", children=[
                            html.A("Set priority", className="navbar-link"),
                            html.Div(className="navbar-dropdown", children=[
                                html.Div(className="navbar-item", children=[
                                    html.Label("Priority:", htmlFor="static-sched-priority"),
                                ]),
                                html.Div(className="navbar-item", children=[
                                    dcc.Input(id="static-sched-priority", type="number", className="input"),
                                ]),
                                html.Div(className="navbar-item", children=[
                                    html.Button("Set priority", type="button", id="static-sched-set-priority",
                                                className="button")
                                ])
                            ])
                        ])
                    ]),
                    html.Div(className="navbar-end", children=[
                        html.Div(className="navbar-item", children=[
                            html.Button("Rerun simulation", id="static-sched-rerun", className="button is-primary")
                        ])
                    ])
                ])
            ]),
            dcc.Graph(id="gantt", figure=self._make_gantt()),
            html.Div(className="columns is-variable is-8 is-centered is-narrow", children=[
                html.Div(className="column", children=[
                    html.H3("Selected task", className="title is-3 has-text-centered"),
                    html.Div([
                        html.B("Name: "), html.Span(id="task-info-name"), html.Br(),
                        html.B("SubmitOrder: "), html.Span(id="task-info-submitorder"), html.Br(),
                        html.B("Worker: "), html.Span(id="task-info-worker"), html.Br()
                    ], className="box")
                ]),
                html.Div(className="column", children=[
                    html.H3("Constraints", className="title is-3 has-text-centered"),
                    html.Table([
                        html.Thead([html.Tr([html.Th("SubmitOrder"), html.Th("Constraint"), html.Th("")])]),
                        html.Tbody(self._get_full_constraints_table(), id="static-sched-summary-body")
                    ], className="table is-fullwidth is-hoverable")
                ]),
                html.Div(className="column", children=[
                    html.H3("History", className="title is-3 has-text-centered"),
                    html.Table([
                        html.Thead([html.Tr([html.Th("Time"), html.Th("Rollback")])]),
                        html.Tbody(self._get_full_rollback_table(), id="static-sched-history")
                    ], className="table is-fullwidth is-hoverable")
                ])
            ]),
            html.Div(id={"type": "None", "index": 0})
        ])
        self._define_callbacks()

    def _get_workers_dropdown_list(self):
        return [{"label": val, "value": i} for i, val in self.simu.workers.items()]

    def _get_constraint_summary_row(self, index: int, c: Constraint):
        if isinstance(c, WorkerConstraint):
            return html.Tr([
                html.Td(c.submit_order, className="is-vcentered"),
                html.Td([
                    html.Label("Worker:"),
                    dcc.Dropdown(
                        self._get_workers_dropdown_list(),
                        id={"type": "static-sched-workerid", "index": index},
                        style={"width": "150px"},
                        value=c.workerid
                    )
                ], className="is-vcentered"),
                html.Td(
                    html.Button("X", id={"type": "static-sched-remove", "index": index}, className="delete is-large"),
                    className="is-vcentered")
            ], id={"type": "static-sched-row", "index": index})
        elif isinstance(c, DependencyConstraint):
            return html.Tr([
                html.Td(c.submit_order, className="is-vcentered"),
                html.Td([
                    html.Label("Dependency:"), html.Br(),
                    dcc.Input(
                        id={"type": "static-sched-parent", "index": index},
                        style={"width": "100px"},
                        type="number",
                        value=c.parent,
                        className="input"
                    )
                ], className="is-vcentered"),
                html.Td(
                    html.Button("X", id={"type": "static-sched-remove", "index": index}, className="delete is-large"),
                    className="is-vcentered")
            ], id={"type": "static-sched-row", "index": index})
        elif isinstance(c, PriorityConstraint):
            return html.Tr([
                html.Td(c.submit_order),
                html.Td([
                    html.Label("Priority:"), html.Br(),
                    dcc.Input(
                        id={"type": "static-sched-priority", "index": index},
                        style={"width": "100px"},
                        type="number",
                        value=c.priority,
                        className="input"
                    )
                ]),
                html.Td(
                    html.Button("X", id={"type": "static-sched-remove", "index": index}, className="delete is-large"),
                    className="is-vcentered")
            ], id={"type": "static-sched-row", "index": index})

    def _get_full_constraints_table(self):
        return [self._get_constraint_summary_row(i, c) for i, c in enumerate(self.simu.static_sched.constraints)]

    def _get_rollback_table_row(self):
        return html.Tr([
            html.Td(self.simu.static_sched.get_last_exec_time(), className="is-vcentered"),
            html.Td(html.Button("Rollback", id={
                "type": "static-sched-history-rollback",
                "index": len(self.simu.static_sched.history) - 1
            }, className="button is-primary is-outlined is-vcentered"))
        ])

    def _get_full_rollback_table(self):
        children = []
        for i, h in enumerate(self.simu.static_sched.history):
            children.append(
                html.Tr([
                    html.Td(h["time"], className="is-vcentered"),
                    html.Td(html.Button("Rollback", id={
                        "type": "static-sched-history-rollback",
                        "index": i
                    }, className="button is-primary is-outlined is-vcentered"))
                ])
            )
        return children

    def _define_callbacks(self):
        @self.app.callback(
            Output(component_id="gantt", component_property="figure", allow_duplicate=True),
            Output(component_id="static-sched-history", component_property="children"),
            Output(component_id="static-sched-dependency-click", component_property="value", allow_duplicate=True),
            Output(component_id="static-sched-summary-body", component_property="children", allow_duplicate=True),
            Output(component_id="notification", component_property="data", allow_duplicate=True),
            Input(component_id="gantt", component_property="clickData"),
            State(component_id="static-sched-dependency-click", component_property="value"),
            State(component_id="static-sched-submitorder", component_property="value"),
            Input(component_id="static-sched-rerun", component_property="n_clicks"),
            Input(component_id={"type": "static-sched-history-rollback", "index": ALL}, component_property="n_clicks"),
            prevent_initial_call=True
        )
        def update_gantt(click_data, add_dependency_click, selected_submit_order, _, __):
            patch = Patch()
            patched_summary = Patch()
            if type(ctx.triggered_id) is not str and ctx.triggered_id["type"] == "static-sched-history-rollback":
                self.simu.run()
                return self._make_gantt(), patch, "false", patched_summary, no_update
            elif ctx.triggered_id == "gantt":
                if add_dependency_click == "false":
                    return self._get_gantt_with_dependencies(
                        click_data["points"][0]["customdata"][1],
                        click_data["points"][0]["base"],
                        click_data["points"][0]["x"],
                        click_data["points"][0]["y"],
                        click_data["points"][0]["customdata"][2]
                    ), patch, no_update, patched_summary, no_update
                else:
                    submit_order = int(selected_submit_order)
                    parent = click_data["points"][0]["customdata"][2]
                    if self.simu.graph.check_for_cyclic_dependencies(parent, submit_order, self.simu.static_sched):
                        return no_update, no_update, "false", no_update, \
                            "Cyclic dependency detected. The constraint has not been created."
                    c = DependencyConstraint(submit_order, parent)
                    index = self.simu.static_sched.add_constraint(c)
                    patched_summary.append(self._get_constraint_summary_row(index, c))
                    return no_update, no_update, "false", patched_summary, no_update

            elif ctx.triggered_id == "static-sched-rerun":
                self.simu.run()
                patch.append(self._get_rollback_table_row())
                return self._make_gantt(), patch, no_update, patched_summary, no_update
            raise PreventUpdate

        @self.app.callback(
            Output(component_id={"type": "static-sched-history-rollback", "index": MATCH},
                   component_property="children"),
            Input(component_id={"type": "static-sched-history-rollback", "index": MATCH},
                  component_property="n_clicks"),
            State(component_id={"type": "static-sched-history-rollback", "index": MATCH},
                  component_property="children"),
            prevent_inital_call=True
        )
        def static_sched_rollback(n_clicks, r):
            if n_clicks is not None and ctx.triggered_id is not None:
                self.simu.static_sched.rollback(ctx.triggered_id["index"])
                return r
            raise PreventUpdate

        @self.app.callback(
            Output(component_id="task-info-name", component_property="children"),
            Output(component_id="task-info-submitorder", component_property="children"),
            Output(component_id="task-info-worker", component_property="children"),
            Output(component_id="static-sched-submitorder", component_property="value"),
            Input(component_id="gantt", component_property="clickData"),
            prevent_initial_call=True
        )
        def get_task_infos(click_data):
            task = click_data["points"][0]
            return (
                task["customdata"][0],  # value (name/type)
                task["customdata"][2],  # submitorder
                self.simu.get_worker_name(task["y"]),  # workerid
                task["customdata"][2],  # submitorder
            )

        @self.app.callback(
            Output(component_id="static-sched-summary-body", component_property="children"),
            Input(component_id={"type": "static-sched-history-rollback", "index": ALL}, component_property="children"),
            Input(component_id={"type": "static-sched-remove", "index": ALL}, component_property="children"),
            prevent_initial_call=True
        )
        def update_summary(_, __):
            return [self._get_constraint_summary_row(i, c) for i, c in self.simu.static_sched.constraints.items()]

        @self.app.callback(
            Output(component_id="static-sched-summary-body", component_property="children", allow_duplicate=True),
            Output(component_id="static-sched-dependency-click", component_property="value", allow_duplicate=True),
            Output(component_id="notification", component_property="data", allow_duplicate=True),
            Input(component_id="static-sched-assign", component_property="n_clicks"),
            Input(component_id="static-sched-add-dependency", component_property="n_clicks"),
            Input(component_id="static-sched-add-dependency-click", component_property="n_clicks"),
            Input(component_id="static-sched-set-priority", component_property="n_clicks"),
            State(component_id="static-sched-submitorder", component_property="value"),
            State(component_id="static-sched-workerid", component_property="value"),
            State(component_id="static-sched-parent", component_property="value"),
            State(component_id="static-sched-priority", component_property="value"),
            prevent_initial_call=True
        )
        def add_constraint(click_assign, click_add_dependency, click_add_dependency_click, click_set_priority,
                           submit_order, workerid, parent_submitorder, priority):
            patched_summary = Patch()
            if ctx.triggered_id == "static-sched-assign":
                if click_assign is None:
                    raise PreventUpdate
                c = WorkerConstraint(submit_order, workerid)
                index = self.simu.static_sched.add_constraint(c)
                patched_summary.append(self._get_constraint_summary_row(index, c))

            elif ctx.triggered_id == "static-sched-add-dependency":
                if click_add_dependency is None:
                    raise PreventUpdate
                if self.simu.graph.check_for_cyclic_dependencies(parent_submitorder, submit_order,
                                                                 self.simu.static_sched):
                    return no_update, no_update, "Cyclic dependency detected. The constraint has not been created."
                c = DependencyConstraint(submit_order, parent_submitorder)
                index = self.simu.static_sched.add_constraint(c)
                patched_summary.append(self._get_constraint_summary_row(index, c))

            elif ctx.triggered_id == "static-sched-add-dependency-click":
                if click_add_dependency_click is None:
                    raise PreventUpdate
                return no_update, "true", no_update

            elif ctx.triggered_id == "static-sched-set-priority":
                if click_set_priority is None:
                    raise PreventUpdate
                c = PriorityConstraint(submit_order, priority)
                index = self.simu.static_sched.add_constraint(c)
                patched_summary.append(self._get_constraint_summary_row(index, c))

            return patched_summary, no_update, no_update

        @self.app.callback(
            Output(component_id={"type": "static-sched-remove", "index": MATCH}, component_property="children"),
            Input(component_id={"type": "static-sched-remove", "index": MATCH}, component_property="n_clicks"),
        )
        def remove_constraint(n_clicks):
            if n_clicks is None:
                raise PreventUpdate
            self.simu.static_sched.remove_constraint(ctx.triggered_id["index"])
            return None

        @self.app.callback(
            Output(component_id={"type": "static-sched-workerid", "index": MATCH},
                   component_property="children",
                   allow_duplicate=True),
            Input(component_id={"type": "static-sched-workerid", "index": MATCH}, component_property="value"),
            State(component_id={"type": "static-sched-workerid", "index": MATCH}, component_property="id"),
            prevent_initial_call=True
        )
        def edit_worker_constraint_workerid(value, index):
            if index is not None:
                self.simu.static_sched.edit_constraint(index["index"], workerid=value)
            raise PreventUpdate

        @self.app.callback(
            Output(component_id={"type": "static-sched-parent", "index": MATCH},
                   component_property="children",
                   allow_duplicate=True),
            Input(component_id={"type": "static-sched-parent", "index": MATCH}, component_property="value"),
            State(component_id={"type": "static-sched-parent", "index": MATCH}, component_property="id"),
            prevent_initial_call=True
        )
        def edit_dependency_constraint_parent(value, index):
            if index is not None:
                self.simu.static_sched.edit_constraint(index["index"], parent=value)
            raise PreventUpdate

        @self.app.callback(
            Output(component_id={"type": "static-sched-priority", "index": MATCH},
                   component_property="children",
                   allow_duplicate=True),
            Input(component_id={"type": "static-sched-priority", "index": MATCH}, component_property="value"),
            State(component_id={"type": "static-sched-priority", "index": MATCH}, component_property="id"),
            prevent_initial_call=True
        )
        def edit_priority_constraint_priority(value, index):
            if index is not None:
                self.simu.static_sched.edit_constraint(index["index"], priority=value)
            raise PreventUpdate

        self.app.clientside_callback(
            """
            function (message) {
                if (message) alert(message);
                return "";
            }
            """,
            Output(component_id="notification", component_property="data", allow_duplicate=True),
            Input(component_id="notification", component_property="data"),
            prevent_initial_callback=True
        )

    def run(self):
        self.app.run_server()

    def _make_gantt(self, jobid=-1) -> go.Figure:
        fig = go.Figure()
        for e in self.simu.gantt_data:
            fig.add_trace(go.Bar(
                orientation="h",
                base=e["base"],
                x=e["x"],
                y=e["y"],
                name=e["name"],
                offset=-0.5,
                width=0.8,
                marker={
                    "pattern": {"shape": e["pattern"]},
                    "line": {
                        "width": np.where(e["customdata"]["JobId"] == jobid, 2, 0),
                        "color": "black"
                    }
                },
                customdata=e["customdata"],
                hovertemplate=("<extra></extra><b>Type:</b> %{customdata[0]}<br>"
                               "<b>JobID:</b> %{customdata[1]}<br>"
                               "<b>SubmitOrder:</b> %{customdata[2]}<br>"
                               "<b>Priority:</b> %{customdata[5]}<br>"
                               "<b>Tag:</b> %{customdata[6]}<br>"
                               "<b>Start:</b> %{customdata[3]:.6f}<br>"
                               "<b>End:</b> %{customdata[4]:.6f}<br>"
                               "<b>Slack:</b> %{customdata[7]:.2f}%")
            ))
        fig.update_layout(
            xaxis={"range": [-1, self.simu.get_current_execution_time()+1]},
            yaxis={"ticktext": list(self.simu.workers.values()), "tickvals": list(self.simu.workers.keys())},
            uirevision="never"
        )
        return fig

    def _get_gantt_with_dependencies(self, jobid, start, end, y, submitorder) -> go.Figure:
        # Init
        arrows = []
        trace_end = self.simu.get_current_execution_time()

        # Parents
        parents = self.simu.get_parents(jobid)
        for _, row in parents.iterrows():
            arrows.append(go.layout.Annotation(
                x=start,
                y=y,
                xref="x", yref="y",
                ax=row["EndTime"],
                ay=row["WorkerId"],
                axref="x", ayref="y",
                text="",
                showarrow=True,
                arrowhead=2,
                arrowwidth=2,
                arrowcolor="#fce300"
            ))
        lower_bound = max(parents["EndTime"], default=0)

        # Children
        children = self.simu.get_children(jobid)
        for i, row in children.iterrows():
            arrows.append(go.layout.Annotation(
                x=row["StartTime"],
                y=row["WorkerId"],
                xref="x", yref="y",
                ax=end,
                ay=y,
                axref="x", ayref="y",
                text="",
                showarrow=True,
                arrowhead=2,
                arrowwidth=2,
                arrowcolor="#fce300"
            ))
        upper_bound = min(children["StartTime"], default=trace_end)

        # Constraints
        parents = self.simu.get_constraint_parents(submitorder)
        for _, row in parents.iterrows():
            arrows.append(go.layout.Annotation(
                x=start,
                y=y,
                xref="x", yref="y",
                ax=row["EndTime"],
                ay=row["WorkerId"],
                axref="x", ayref="y",
                text="",
                showarrow=True,
                arrowhead=2,
                arrowwidth=2,
                arrowcolor="#ff0000"
            ))
        lower_bound = max([lower_bound, max(parents["EndTime"], default=0)])  # take manual dependencies into account

        gantt = self._make_gantt(jobid).update_layout(annotations=arrows)
        gantt = gantt.add_vrect(x0=-99, x1=lower_bound, line_width=0, fillcolor="black", opacity=0.4)
        gantt = gantt.add_vrect(x0=upper_bound, x1=trace_end*10, line_width=0, fillcolor="black", opacity=0.4)
        return gantt
