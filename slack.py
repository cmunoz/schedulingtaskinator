import pandas as pd
import numpy as np
import networkx as nx
from perf_model import PerfModel
from static_sched import StaticSched, DependencyConstraint


class Graph:
    def __init__(self, df_app: pd.DataFrame, df_deps: pd.DataFrame, perf_model: PerfModel):
        self.df_app = df_app

        self.graph = nx.DiGraph()
        self.graph.add_nodes_from(self.df_app["SubmitOrder"])

        self.df_deps = df_deps.drop_duplicates()
        self.df_deps = self.df_deps \
            .merge(self.df_app[["JobId", "SubmitOrder"]], how="left", left_on="JobId", right_on="JobId") \
            .merge(self.df_app[["JobId", "SubmitOrder"]], how="left", left_on="DependsOn", right_on="JobId") \
            .dropna()
        self.df_deps = self.df_deps[["SubmitOrder_x", "SubmitOrder_y"]] \
            .rename(columns={"SubmitOrder_x": "SubmitOrder", "SubmitOrder_y": "DependsOn"})

        self.graph.add_edges_from(
            list(self.df_deps.loc[:, ["DependsOn", "SubmitOrder"]].itertuples(index=False, name=None))
        )

        self.sources = self.df_app.loc[~self.df_app["SubmitOrder"].isin(self.df_deps["SubmitOrder"]), "SubmitOrder"]\
                           .to_list()
        self.leaves = self.df_app.loc[~self.df_app["SubmitOrder"].isin(self.df_deps["DependsOn"]), "SubmitOrder"]\
                          .to_list()

        self.tasks = pd.DataFrame({
            "SubmitOrder": self.df_app["SubmitOrder"],
            # Duration = Best time available in model
            "Duration": [perf_model.get_best_time(x["Name"], x["Footprint"]) for _, x in self.df_app.iterrows()],
            "Name": self.df_app["Name"],
            "EST": np.zeros(self.df_app.shape[0]),  # Earliest Start Time (asap scheduling)
            "LST": np.zeros(self.df_app.shape[0]),  # Latest Start Time (alap scheduling)
            "Slack": np.zeros(self.df_app.shape[0])
        })
        self.tasks = self.tasks.set_index("SubmitOrder")

    def _asap(self):
        order = list(nx.dfs_postorder_nodes(self.graph))
        order.reverse()
        for t in self.sources:
            self.tasks.loc[t, "EST"] = 0.0

        for s in order:
            parents = self.df_deps.loc[self.df_deps["SubmitOrder"] == s, "DependsOn"].to_list()
            if len(parents) == 0:
                continue
            self.tasks.loc[s, "EST"] = max([self.tasks.loc[p, "EST"]+self.tasks.loc[p, "Duration"] for p in parents])

    def _alap(self):
        g = self.graph.copy().reverse()
        order = list(nx.dfs_postorder_nodes(g))
        order.reverse()
        for t in self.leaves:
            self.tasks.loc[t, "LST"] = self.tasks.loc[t, "EST"]

        for s in order:
            parents = self.df_deps.loc[self.df_deps["DependsOn"] == s, "SubmitOrder"].to_list()
            if len(parents) == 0:
                continue
            self.tasks.loc[s, "LST"] = min([self.tasks.loc[p, "LST"] for p in parents]) - self.tasks.loc[s, "Duration"]

    def compute_slack(self):
        self._asap()
        self._alap()
        self.tasks["Slack"] = (self.tasks["LST"] - self.tasks["EST"]) / max(self.df_app["EndTime"]) * 100.0

    def get_slack_df(self):
        return self.tasks.reset_index()[["SubmitOrder", "Slack"]]

    def check_for_cyclic_dependencies(self, parent_submitorder: int, child_submitorder: int, static_sched: StaticSched):
        """Check for cyclic dependencies on the task graph

        :param parent_submitorder: SubmitOrder of the parent of the new dependency
        :param child_submitorder: SubmitOrder of the child of the new dependency
        :param static_sched: existing StaticSched

        :returns: true if there is a cycle, else false
        """
        test_graph = self.graph.copy()
        test_graph.add_edge(parent_submitorder, child_submitorder)
        test_graph.add_edges_from([(s.parent, s.submit_order)
                                   for s in static_sched.constraints.values()
                                   if isinstance(s, DependencyConstraint)])
        try:
            nx.find_cycle(test_graph)
            # if we're still here, a cycle has been found
            return True
        except nx.NetworkXNoCycle:
            return False


if __name__ == "__main__":
    from simu import Simu
    try:
        import graphviz
    except ImportError:
        print("Please install graphviz module to run this script")
        exit(1)
    simu = Simu("./tasks_small.rec", hostname="attila")
    model = PerfModel("attila")
    graph = Graph(simu.df_app, simu.df_deps, model)
    graph.compute_slack()

    # export
    g = graphviz.Digraph()
    for _, t in graph.tasks.iterrows():
        g.node(str(int(t["Index"])), f"{t['Name']}\nSlack:{round(t['Slack'], 2)}")

    for d in graph.df_deps.itertuples():
        g.edge(str(int(d.DependsOn)), str(int(d.SubmitOrder)))

    g.render(format="png")
